﻿angular.module("pouchapp", [])
 
.run(function($pouchDB) {
    $pouchDB.setDatabase("local-test");
    $pouchDB.sync("http://localhost:5984/remote-test");

})
 
.config(function() {
 
})
 
.controller("mainController", function($scope, $rootScope, $pouchDB) {
    $scope.items = {};
    $scope.sync = false;
    $scope.loading = true;
    $pouchDB.startListening();

    $rootScope.$on("$pouchDB:change", function (event, data) {
        $scope.items[data.doc._id] = data.doc;
        $scope.$apply();
    });


    $rootScope.$on("$pouchDB:unsync", function (event, data) {
        $scope.sync = false;
        $scope.loading = false;
        $scope.$apply();
    });

    $rootScope.$on("$pouchDB:sync", function (event, data) {
        $scope.sync = true;
        $scope.loading = false;
        $scope.$apply();
    });
    
    $scope.save = function () {
        if (!$scope.username) {
            alert("Debe identificarse");
            return;
        }
        if (!$scope.msg) {
            alert("Debe escribir un mensaje");
            return;
        }
        var jsonDocument = {
            "message": $scope.msg,
            "user":$scope.username,
             "date": new Date()
        };

           jsonDocument["_id"] = $scope.items.length+1;
            //jsonDocument["_rev"] = $stateParams.documentRevision;

        $pouchDB.save(jsonDocument).then(function (response) {
            
        }, function (error) {
            console.log("ERROR -&gt; " + error);
        });
    }

 
})

 .service("$pouchDB", ["$rootScope", "$q", function ($rootScope, $q) {

     var database;
     
        var changeListener;
     var dbname;
        this.setDatabase = function (databaseName) {
            database = new PouchDB(databaseName);
            dbname = databaseName;
        }

        this.startListening = function () {
            changeListener = database.changes({
                live: true,
                include_docs: true
            }).on("change", function (change) {

                $rootScope.$broadcast("$pouchDB:change", change);

            });
        }

        this.stopListening = function () {
            changeListener.cancel();
        }

     this.sync = function (remoteDatabase) {

         let options = {
             live: true,retry:true
         };
         //database.sync(remoteDatabase, options)
         //    .on('change', function (a) {

         //    })
         //    .on('paused', function (a) {

         //    })
         //    .on('active', function (a) {

         //    });
         database.replicate.from(remoteDatabase).on('complete', function (info) {
             // then two-way, continuous, retriable sync
             $rootScope.$broadcast("$pouchDB:sync");
             database.sync(remoteDatabase, options)
                 .on('change', function (a){

                  })
                 .on('paused', function (a) {
                     
                 })
                 .on('active', function (a) {
                     
                 });
         }).on('error', function (a) {
             $rootScope.$broadcast("$pouchDB:unsync");
         });







         //var sync = PouchDB.sync(dbname, remoteDatabase)
             
         //.on('complete', function (info) {
         //    $rootScope.$broadcast("$pouchDB:sync");

            

         //    PouchDB.sync(dbname, remoteDatabase, options)
         //        .on('paused', function (err) {
         //            // replication paused (e.g. replication up to date, user went offline)
         //            //$rootScope.$broadcast("$pouchDB:unsync");
         //        }).on('active', function (info,b) {
         //            $rootScope.$broadcast("$pouchDB:sync");
                   
         //        });








         //}).on('error', function (err) {
         //    $rootScope.$broadcast("$pouchDB:unsync");
         //});
          
        }

        this.save = function (jsonDocument) {
            var deferred = $q.defer();
            if (!jsonDocument._id) {
                database.post(jsonDocument).then(function (response) {
                    deferred.resolve(response);
                }).catch(function (error) {
                    deferred.reject(error);
                });
            } else {
                database.put(jsonDocument).then(function (response) {
                    deferred.resolve(response);
                }).catch(function (error) {
                    deferred.reject(error);
                });
            }
            return deferred.promise;
        }

        this.delete = function (documentId, documentRevision) {
            return database.remove(documentId, documentRevision);
        }

        this.get = function (documentId) {
            return database.get(documentId);
        }

        this.destroy = function () {
            database.destroy();
        }

    }]);